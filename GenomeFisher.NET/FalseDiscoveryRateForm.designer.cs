﻿namespace GenomeFisher.NET
{
    partial class FalseDiscoveryRateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FalseDiscoveryRateForm));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkFiltering = new System.Windows.Forms.CheckBox();
            this.chkPFDR = new System.Windows.Forms.CheckBox();
            this.lblPI = new System.Windows.Forms.Label();
            this.cmbPIMethod = new System.Windows.Forms.ComboBox();
            this.udSampleSize = new System.Windows.Forms.NumericUpDown();
            this.chkAutomatedSampling = new System.Windows.Forms.CheckBox();
            this.lblSampleSize = new System.Windows.Forms.Label();
            this.chkSampling = new System.Windows.Forms.CheckBox();
            this.cmdCompute = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udSampleSize)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkFiltering);
            this.groupBox2.Controls.Add(this.chkPFDR);
            this.groupBox2.Controls.Add(this.lblPI);
            this.groupBox2.Controls.Add(this.cmbPIMethod);
            this.groupBox2.Controls.Add(this.udSampleSize);
            this.groupBox2.Controls.Add(this.chkAutomatedSampling);
            this.groupBox2.Controls.Add(this.lblSampleSize);
            this.groupBox2.Controls.Add(this.chkSampling);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(323, 214);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Operation properties";
            // 
            // chkFiltering
            // 
            this.chkFiltering.AutoSize = true;
            this.chkFiltering.Location = new System.Drawing.Point(17, 31);
            this.chkFiltering.Name = "chkFiltering";
            this.chkFiltering.Size = new System.Drawing.Size(125, 17);
            this.chkFiltering.TabIndex = 8;
            this.chkFiltering.Text = "Filter irrelevant tables";
            this.chkFiltering.UseVisualStyleBackColor = true;
            this.chkFiltering.CheckedChanged += new System.EventHandler(this.ChkFilteringCheckedChanged);
            // 
            // chkPFDR
            // 
            this.chkPFDR.AutoSize = true;
            this.chkPFDR.Location = new System.Drawing.Point(17, 87);
            this.chkPFDR.Name = "chkPFDR";
            this.chkPFDR.Size = new System.Drawing.Size(170, 17);
            this.chkPFDR.TabIndex = 7;
            this.chkPFDR.Text = "ComputeHypergeometricProbability Positive FDR (pFDR)";
            this.chkPFDR.UseVisualStyleBackColor = true;
            // 
            // lblPI
            // 
            this.lblPI.AutoSize = true;
            this.lblPI.Location = new System.Drawing.Point(17, 59);
            this.lblPI.Name = "lblPI";
            this.lblPI.Size = new System.Drawing.Size(107, 13);
            this.lblPI.TabIndex = 6;
            this.lblPI.Text = "PI evaluation method";
            // 
            // cmbPIMethod
            // 
            this.cmbPIMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPIMethod.FormattingEnabled = true;
            this.cmbPIMethod.Items.AddRange(new object[] {
            "PI = 1",
            "sum observed p-values / expected p-value",
            "2*avg(p-value)"});
            this.cmbPIMethod.Location = new System.Drawing.Point(142, 56);
            this.cmbPIMethod.Name = "cmbPIMethod";
            this.cmbPIMethod.Size = new System.Drawing.Size(175, 21);
            this.cmbPIMethod.TabIndex = 5;
            // 
            // udSampleSize
            // 
            this.udSampleSize.Enabled = false;
            this.udSampleSize.Location = new System.Drawing.Point(155, 132);
            this.udSampleSize.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.udSampleSize.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.udSampleSize.Name = "udSampleSize";
            this.udSampleSize.Size = new System.Drawing.Size(95, 20);
            this.udSampleSize.TabIndex = 3;
            this.udSampleSize.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // chkAutomatedSampling
            // 
            this.chkAutomatedSampling.AutoSize = true;
            this.chkAutomatedSampling.Enabled = false;
            this.chkAutomatedSampling.Location = new System.Drawing.Point(36, 155);
            this.chkAutomatedSampling.Name = "chkAutomatedSampling";
            this.chkAutomatedSampling.Size = new System.Drawing.Size(121, 17);
            this.chkAutomatedSampling.TabIndex = 2;
            this.chkAutomatedSampling.Text = "Automated sampling";
            this.chkAutomatedSampling.UseVisualStyleBackColor = true;
            // 
            // lblSampleSize
            // 
            this.lblSampleSize.AutoSize = true;
            this.lblSampleSize.Enabled = false;
            this.lblSampleSize.Location = new System.Drawing.Point(36, 134);
            this.lblSampleSize.Name = "lblSampleSize";
            this.lblSampleSize.Size = new System.Drawing.Size(100, 13);
            this.lblSampleSize.TabIndex = 1;
            this.lblSampleSize.Text = "Sample size (tables)";
            // 
            // chkSampling
            // 
            this.chkSampling.AutoSize = true;
            this.chkSampling.Location = new System.Drawing.Point(17, 113);
            this.chkSampling.Name = "chkSampling";
            this.chkSampling.Size = new System.Drawing.Size(89, 17);
            this.chkSampling.TabIndex = 0;
            this.chkSampling.Text = "Use sampling";
            this.chkSampling.UseVisualStyleBackColor = true;
            this.chkSampling.CheckedChanged += new System.EventHandler(this.ChkSamplingCheckedChanged);
            // 
            // cmdCompute
            // 
            this.cmdCompute.Location = new System.Drawing.Point(117, 232);
            this.cmdCompute.Name = "cmdCompute";
            this.cmdCompute.Size = new System.Drawing.Size(96, 24);
            this.cmdCompute.TabIndex = 9;
            this.cmdCompute.Text = "ComputeHypergeometricProbability";
            this.cmdCompute.UseVisualStyleBackColor = true;
            this.cmdCompute.Click += new System.EventHandler(this.BtnComputeClick);
            // 
            // FalseDiscoveryRateForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(348, 261);
            this.Controls.Add(this.cmdCompute);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FalseDiscoveryRateForm";
            this.Text = "FDR";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udSampleSize)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkAutomatedSampling;
        private System.Windows.Forms.Label lblSampleSize;
        private System.Windows.Forms.CheckBox chkSampling;
        private System.Windows.Forms.NumericUpDown udSampleSize;
        private System.Windows.Forms.Label lblPI;
        private System.Windows.Forms.ComboBox cmbPIMethod;
        private System.Windows.Forms.Button cmdCompute;
        private System.Windows.Forms.CheckBox chkPFDR;
        private System.Windows.Forms.CheckBox chkFiltering;
    }
}

