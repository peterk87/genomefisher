﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace GenomeFisher.NET
{
/**
* Implements the False Discovery Rate algorithms
* Author - Guy Shani 2008
* */

    internal class FalseDiscoveryRate
    {
        #region PiMethod enum

        public enum PiMethod
        {
            /// <summary>Use pi = 1 (no PI estimation)</summary>
            One,

            /// <summary>Use pi = (\sum_p rho(p)pr(P=p))/(\sum_p rho(p)pr(P=p|H=0))</summary>
            WeightedSum,

            /// <summary>Following Pounds & Cheng - use pi = 2*avg(p)</summary>
            DoubleAverage,

            /// <summary>ComputeHypergeometricProbability weighted sum but only for relevant tables (tables that can possibly be significant)</summary>
            Filtering
        };

        #endregion

        private const double EPSILON = 0.000000000001;
        private static readonly Random MRndGenerator = new Random();
        //private double m_dFDRCutoff; //for a large number of tables - avoid computing tables with large FDR

        private readonly List<ContingencyTable> _actTables;

        private readonly List<FDRData> _fdr;
        private readonly bool _mBContinue;
        private readonly bool _mBPositiveFDR; //compute pFDR (Storey) or regular FDR (Benjamini & Hochberg)
        private readonly double _mDMinimalChange; //Minimal change that still requires additional sampling iterations
        private readonly PiMethod _mPmEvaluatePi; //The type of PI evaluation method

        public FalseDiscoveryRate(double dMinimalChange, PiMethod pmEvaluatePi, bool bPositiveFDR, List<ContingencyTable> ctList)
        {
            _fdr = new List<FDRData>();
            _actTables = ctList;
            _mDMinimalChange = dMinimalChange;
            _mPmEvaluatePi = pmEvaluatePi;
            _mBContinue = true;
            _mBPositiveFDR = bPositiveFDR;
        }

        public List<FDRData> FDRStats { get { return _fdr; } }

        private List<KeyValuePair<double, double>> PtoQMapping { get; set; }

        public static Random RandomGenerator { get { return MRndGenerator; } }

/*
        public double PI
        {
            get
            {
                return _mDPi;
            }
        }
*/

        //Here we force the computation of the Fisher test scores so that we can later sort the tables
        private void ComputeFisherScores()
        {
            foreach (ContingencyTable ct in _actTables)
            {
                ContingencyTable ct1 = ct;
                ThreadPool.QueueUserWorkItem(delegate { ct1.GetFisher2TailPermutationTest(); });
            }
            _actTables.Sort();
        }

        /// <summary>
        /// 1-(1-x)^m
        /// </summary>
        /// <param name="x"></param>
        /// <param name="cTables"></param>
        /// <returns></returns>
        private double OneMinusOneMinuXToTheM(double x, long cTables)
        {
            if (x > 1E-10) //assuming that above this threshold (1-x)^m is reasonable computed. Math.Pow is much faster than the loops below.
                return 1 - Math.Pow(1 - x, cTables);
            long iPower;
            double dSum = 0.0, dValue = 1.0;
            var lValues = new List<double>();
            //dValue is 0 when the accuracy drops below double.Epsilon.
            //This is a reasonably efficient implementation as usually only a small number of values will be higher than double.Epsilon.
            for (iPower = 1; iPower <= cTables && Math.Abs(dValue - 0.0) > EPSILON; iPower++)
            {
                dValue *= (cTables - iPower) / (double) iPower;
                dValue *= -x;
                if (Math.Abs(dValue - 0.0) > EPSILON)
                    lValues.Add(dValue);
            }
            lValues.Reverse();
            foreach (double d in lValues)
            {
                dSum -= d;
            }
            //(1-x)^m = 1 + \sum_i=1..m a_i -x^i
            //1-(1-x)^m = -\sum_i=1..m a_i -x^i
            return dSum;
        }

        /// <summary>
        /// When computing we only add the probabilities of the permutations to the closest higher p-value table. 
        /// Now, we need to go over all these tables and sum everything that has smaller p-value (more significant). 
        /// We take a mapping from p-value to single appearance probabilities and return a mapping from p-value to sum of all more significant probabilities.
        /// </summary>
        /// <param name="slFDR"></param>
        /// <param name="dPiEstimation"></param>
        private void SumFDRs(Map<double, FDRData> slFDR, double dPiEstimation)
        {
            double dSum = 0;
            int iTable;
            long cAllTables = _actTables.Count;
            long cTables = 0;
            double dNextFisherScore = 0.0;

            //First, sum all the pooled p-values that are lower than the current table
            foreach (FDRData data in slFDR.Values)
            {
                dSum += data.PooledPValue;
                data.PooledPValue = dSum;
                data.PooledPValue /= cAllTables;
                if (data.FilteringPi > 0.0)
                    data.FDR = dSum * data.FilteringPi;
                else
                    data.FDR = dSum * dPiEstimation;
                if (_mBPositiveFDR)
                {
                    data.RejectionAreaProb = OneMinusOneMinuXToTheM(data.PooledPValue, cAllTables);
                    data.FDR /= data.RejectionAreaProb;
                }
            }

            //We now have to divide by the number of more significant tables to move from pooled p-values to FDR
            for (iTable = 0; iTable < _actTables.Count; iTable++)
            {
                ContingencyTable ctCurrent = _actTables[iTable];
                ContingencyTable ctNext = iTable < _actTables.Count - 1 ? _actTables[iTable + 1] : null;
                double dFisherScore = Round(ctCurrent.GetFisher2TailPermutationTest());
                cTables++;
                if (ctNext != null)
                    dNextFisherScore = Round(ctNext.GetFisher2TailPermutationTest());
                if ((ctNext == null) || (Math.Abs(dFisherScore - dNextFisherScore) > EPSILON))
                {
                    slFDR[dFisherScore].FDR /= cTables;
                }
            }
        }

        //returns the first key that is greater than dKey
        //implemented usign binary search
        private double GetNextKey(IList<double> lKeys, double dKey)
        {
            return GetNextKey(lKeys, dKey, 0, lKeys.Count - 1);
        }

        //returns the first key that is greater than dKey in the range [start,end]
        //implemented usign binary search
        private double GetNextKey(IList<double> lKeys, double dKey, int iStart, int iEnd)
        {
            if (lKeys[iStart] >= dKey)
                return lKeys[iStart];
            if (lKeys[iEnd] < dKey)
                return lKeys[iEnd + 1];
            if (iEnd - iStart <= 5)
            {
                int idx;
                for (idx = iStart; idx <= iEnd; idx++)
                {
                    if (lKeys[idx] >= dKey)
                        return lKeys[idx];
                }
                return double.NaN;
            }
            int iMedian = (iStart + iEnd) / 2;
            if (Math.Abs(dKey - lKeys[iMedian]) < EPSILON)
                return lKeys[iMedian];
            if (dKey < lKeys[iMedian])
                return GetNextKey(lKeys, dKey, iStart, iMedian - 1);
            return GetNextKey(lKeys, dKey, iMedian + 1, iEnd);
        }

        /*
         * Writes the results of the computation to a file.
         * First line is the headers (if exist) with the new columns added.
         * Each following line is the contingency table with the Fisher scores, FDR and q-value
         * */

        private void GetResults(Map<double, FDRData> slFDR)
        {
            var lPtoQMappings = new List<KeyValuePair<double, double>>();

            foreach (ContingencyTable ctCurrent in _actTables)
            {
                double dFisherTest = ctCurrent.GetFisher2TailPermutationTest();
                double dNextKey = GetNextKey(slFDR.KeyList, dFisherTest);
                FDRData fdCurrent = slFDR[dNextKey];
                _fdr.Add(fdCurrent);
                double dCurrentQValue = Floor(fdCurrent.QValue);
                lPtoQMappings.Add(new KeyValuePair<double, double>(dNextKey, dCurrentQValue));
            }
            PtoQMapping = lPtoQMappings;
        }

        public static double Floor(double x)
        {
            string s = x.ToString("e5");
            string[] a = s.Split(new[] {'e'});
            double y = double.Parse(a[0]);
            s = y + "e" + a[1];
            double z = double.Parse(s);
            return z;
        }

        //Rounding 5 digits after the most significant digit
        private static double Round(double x)
        {
            string s = x.ToString("e5");
            string[] a = s.Split(new[] {'e'});
            double y = double.Parse(a[0]);
            y += 0.00001;
            s = y + "e" + a[1];
            double z = double.Parse(s);
            return z;
        }

        /*
         * Main FDR computation function.
         * Takes as input an array of tables, already sorted by Fisher scores.
         * Outputs a map from p-value to FDR.
         * */

        private Map<double, FDRData> ComputeFDRFromTables()
        {
            int iTable, cTables = _actTables.Count;
            double dSumObservedPValues = 0.0;
            double dSumNullPValues = 0.0, dExpectedNullPValue = 0.0;
            int cObservedPValues = 0;


            Map<double, FDRData> slFDR = initFDRMap(_actTables);

            for (iTable = 0; iTable < cTables && _mBContinue; iTable++)
            {
                ContingencyTable ctCurrent = _actTables[iTable];

                double dCurrentTableFisherTestPValue = ctCurrent.GetFisher2TailPermutationTest();

                dSumObservedPValues += dCurrentTableFisherTestPValue;

                //dEPhiObserved += -Math.Log(1 - 0.99999999 * dCurrentTableFisherTestPValue);


                cObservedPValues++;

                double[,] adScores = ctCurrent.ComputeAllPermutationsScores();
                const int cTableCount = 1;

                for (int iCurrent = 0; iCurrent < adScores.GetLength(0); iCurrent++)
                {
                    double dHyperProbability = adScores[iCurrent, 0];
                    double dFisherScore = adScores[iCurrent, 1];

                    dSumNullPValues += dHyperProbability;
                    dExpectedNullPValue += dFisherScore * dHyperProbability;
                    //dEPhiNull += -Math.Log(1 - 0.99999999 * dFisherScore) * dHyperProbability;

                    double dFirstLargerKey = GetNextKey(slFDR.KeyList, dFisherScore);

                    slFDR[dFirstLargerKey].PooledPValue += (dHyperProbability * cTableCount);
                }
            }

            double dPi = 1.0;
            if ((_mPmEvaluatePi == PiMethod.WeightedSum) || (_mPmEvaluatePi == PiMethod.DoubleAverage))
            {
                if (_mPmEvaluatePi == PiMethod.WeightedSum)
                    dPi = (dSumObservedPValues / cObservedPValues) / (dExpectedNullPValue / dSumNullPValues); // \pi_0 = (\sum_T p(T))/(\sum_T p(T)pr(T|H=0))
                else if (_mPmEvaluatePi == PiMethod.DoubleAverage)
                    dPi = 2.0 * (dSumObservedPValues / cObservedPValues); // \pi_0 = 2 * avg(p)
            }
            else if (_mPmEvaluatePi == PiMethod.Filtering)
            {
                Map<double, double> slPi = ComputeFilteringPi(_actTables, slFDR.KeyList);
                var lKeys = new List<double>(slFDR.Keys);
                foreach (double dKey in lKeys)
                {
                    slFDR[dKey].FilteringPi = slPi[dKey];
                }
            }
            SumFDRs(slFDR, dPi);
            return slFDR;
        }

        /*
         * Computing pi in the filtering case.
         * In this case we compute a different pi for each p-value
         * A table is considered relevant for the pi computation of a p-value p only if its marginals support a p-value that is more extreme than p.
         * */

        private Map<double, double> ComputeFilteringPi(List<ContingencyTable> actTables, List<double> lPValues)
        {
            var slRelevantTables = new Map<double, List<ContingencyTable>>();
            double dSumObservedPValuesInRange = 0.0;
            int cObservedTablesInRange = 0;
            double dSumExpectedNullsInRange = 0;
            double dSumNullProbsInRange = 0.0;
            int iTable;
            var slPi = new Map<double, double>();
            ContingencyTable ctCurrent;


            //We first compute the list of relevant tables.
            //For each table we compute its minimal achievable p-value and add it to the next p-value on the list.
            //Now, the relevant tables are all the tables that belong to a p-value that is more exterme than the current one.
            for (iTable = 0; iTable < actTables.Count && _mBContinue; iTable++)
            {
                ctCurrent = actTables[iTable];
                double dMinimalPossiblePValue = ctCurrent.GetMinimalAchievablePValue();
                double dFirstLargerKey = GetNextKey(lPValues, dMinimalPossiblePValue);
                if (!slRelevantTables.ContainsKey(dFirstLargerKey))
                    slRelevantTables.Add(dFirstLargerKey, new List<ContingencyTable>());
                slRelevantTables[dFirstLargerKey].Add(ctCurrent);
            }

            //We iterate from smallest p-value to largest. The order is important because we want the relevant tables list to grow all the time.
            for (iTable = 0; iTable < actTables.Count && _mBContinue; iTable++)
            {
                ctCurrent = actTables[iTable];

                double dCurrentTableFisherTestPValue = Round(ctCurrent.GetFisher2TailPermutationTest());

                if (slRelevantTables.ContainsKey(dCurrentTableFisherTestPValue))
                {
                    //Now we iterate over the list of relevant tables
                    //Note - a table never becomes irrelevant. Therefore we always accumulate more observations and remove any.
                    foreach (ContingencyTable ctRelevant in slRelevantTables[dCurrentTableFisherTestPValue])
                    {
                        double dFisherScore = ctRelevant.GetFisher2TailPermutationTest();

                        dSumObservedPValuesInRange += dFisherScore;
                        cObservedTablesInRange++;
                        //TODO - calling computeAllPermutationsScores twice - inefficient
                        double[,] adScores = ctRelevant.ComputeAllPermutationsScores();

                        for (int iCurrent = 0; iCurrent < adScores.GetLength(0); iCurrent++)
                        {
                            double dHyperProbability = adScores[iCurrent, 0];
                            dFisherScore = adScores[iCurrent, 1];

                            dSumNullProbsInRange += dHyperProbability;
                            dSumExpectedNullsInRange += dFisherScore * dHyperProbability;
                        }
                    }
                    slRelevantTables.Remove(dCurrentTableFisherTestPValue);
                }
                //After iterating over all the relevant tables we compute the PI for that p-value
                //using the weighted sum method
                slPi[dCurrentTableFisherTestPValue] = (dSumObservedPValuesInRange / cObservedTablesInRange) /
                                                      (dSumExpectedNullsInRange / dSumNullProbsInRange);
            }
            slPi[10.0] = 1.0;
            return slPi;
        }

        private Map<double, FDRData> initFDRMap(List<ContingencyTable> actTables)
        {
            var slFDR = new Map<double, FDRData>();
            int iTable;
            for (iTable = 0; iTable < actTables.Count; iTable++)
            {
                ContingencyTable ctCurrent = actTables[iTable];
                double dFisherScore = Round(ctCurrent.GetFisher2TailPermutationTest());
                if (!slFDR.ContainsKey(dFisherScore))
                {
                    slFDR.Add(dFisherScore, new FDRData(dFisherScore));
                }
            }
            slFDR.Add(10.0, new FDRData(10.0)); // add a last entry with a huge fisher score
            return slFDR;
        }

        //ComputeHypergeometricProbability the maximal difference between two lists of FDR values.
        //We use this in the sampling scenario to determine convergence.
        private double GetMaxDifference(Map<double, FDRData> slOld, Map<double, FDRData> slNew)
        {
            double dDiff = 0.0;
            foreach (var pEntry in slNew)
            {
                double dKey = pEntry.Key;
                double dValue = pEntry.Value.FDR;
                double dOldKey = GetNextKey(slOld.KeyList, dKey);
                if (dKey < 1.0 && dOldKey < 1.0)
                {
                    double dOldValue = slOld[dOldKey].FDR;
                    if (Math.Abs(dOldValue - dValue) > dDiff)
                    {
                        dDiff = Math.Abs(dOldValue - dValue);
                    }
                }
            }
            return dDiff;
        }

        /*
         * The main function for computing FDR.
         * Implements the sampling iterations in the case of a huge dataset.
         * */

        public void ComputeFDR()
        {
            Map<double, FDRData> slFDR = null;
            double dMaxDiff = double.PositiveInfinity;

            do //sampling iterations
            {
                Map<double, FDRData> slFDRPrevious = slFDR;

                //compute p-values and sort the tables
                ComputeFisherScores();

                //compute the FDR
                slFDR = ComputeFDRFromTables();

                //ComputeHypergeometricProbability the maximal change in FDR (convergence of sampling process)
                if (slFDRPrevious != null)
                    dMaxDiff = GetMaxDifference(slFDRPrevious, slFDR);

                //Double the sample size
            } //if we are in sampling mode, and the FDR has not yet converged, run another iteration
            while ((_mDMinimalChange > 0.0) && (dMaxDiff > _mDMinimalChange));
            //ComputeHypergeometricProbability the q-values - the minimal FDR over all the rejection areas
            computeQValues(slFDR);
            GetResults(slFDR);
        }

        //q-value is the minimal FDR over all the rejection areas that a table belongs to
        private void computeQValues(Map<double, FDRData> slFDR)
        {
            double dMin = double.PositiveInfinity;
            int iTable = slFDR.Count - 1;
            var lKeys = new List<double>(slFDR.Keys);
            while (iTable >= 0)
            {
                double dFisherScore = lKeys[iTable];
                double dFDR = slFDR[dFisherScore].FDR;
                if (dFDR < dMin)
                    dMin = dFDR;
                slFDR[dFisherScore].QValue = dMin;
                iTable--;
            }
        }
    }
}