﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GenomeFisher.NET
{
    public class FETGroup
    {
        private readonly GenomeFisherAnalysis _analysis;
        private readonly List<ContingencyTable> _contingencyTables = new List<ContingencyTable>();
        private readonly List<GeneGroup> _geneGroups = new List<GeneGroup>();

        /// <summary>Group A information - strains and name.</summary>
        private readonly Group _groupA;

        /// <summary>Group B information - strains and name.</summary>
        private readonly Group _groupB;

        private readonly GroupCollection _groupCollection;

        /// <summary>Threshold for determining what is positive or negative with the numerical data.</summary>
        private readonly double _threshold;

        private double _minPValue;
        private double _mostExtremePValue;

        /// <summary>Constructor function for FETGroup comparison group.</summary>
        /// <param name="groupCollection">GroupCollection that this new FETGroup will be stored in.</param>
        /// <param name="grpA">Group A info.</param>
        /// <param name="grpB">Group B info.</param>
        /// <param name="pvalThreshold">Threshold for significant p-values.</param>
        /// <param name="analysis">GenomeFisherAnalysis object.</param>
        public FETGroup(GenomeFisherAnalysis analysis, GroupCollection groupCollection, Group grpA, Group grpB, double pvalThreshold)
        {
            PValueThreshold = pvalThreshold;

            _analysis = analysis;
            _groupCollection = groupCollection;
            _groupA = grpA;
            _groupB = grpB;

            _threshold = _analysis.Threshold;

            GetCounts();
            GetMostExtremePValue();
        }

        /// <summary>Constructor function for FETGroup comparison group.</summary>
        /// <param name="groupCollection">GroupCollection that this new FETGroup will be stored in.</param>
        /// <param name="grpA">Group A info.</param>
        /// <param name="grpB">Group B info.</param>
        /// <param name="pvalThreshold">Threshold for significant p-values.</param>
        /// <param name="analysis">GenomeFisherAnalysis object.</param>
        /// <param name="hasMultipleThresholds">Dummy argument for analyses with a different threshold for each marker.</param>
        public FETGroup(GenomeFisherAnalysis analysis, GroupCollection groupCollection, Group grpA, Group grpB, double pvalThreshold, bool hasMultipleThresholds)
        {
            PValueThreshold = pvalThreshold;

            _analysis = analysis;
            _groupCollection = groupCollection;
            _groupA = grpA;
            _groupB = grpB;

            GetCounts(true);
            GetMostExtremePValue();
        }

        /// <summary>Constructor for FETGroup class when creating FETGroups from work file.</summary>
        /// <param name="groupCollection"> </param>
        /// <param name="grpA"></param>
        /// <param name="grpB"></param>
        /// <param name="threshold"></param>
        /// <param name="pvalThreshold"></param>
        /// <param name="mostExtremepVal"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        /// <param name="geneIndex"> </param>
        /// <param name="analysis"> </param>
        /// <param name="pvalues"> </param>
        public FETGroup(GenomeFisherAnalysis analysis,
                        GroupCollection groupCollection,
                        Group grpA,
                        Group grpB,
                        double threshold,
                        double pvalThreshold,
                        double mostExtremepVal,
                        List<int> a,
                        List<int> b,
                        List<int> c,
                        List<int> d,
                        List<int> geneIndex,
                        List<double> pvalues)
        {
            _analysis = analysis;
            _groupCollection = groupCollection;
            _groupA = grpA;
            _groupB = grpB;
            _threshold = threshold;
            PValueThreshold = pvalThreshold;
            _mostExtremePValue = mostExtremepVal;
            for (int i = 0; i < a.Count; i++)
            {
                _contingencyTables.Add(GetContingencyTable(a[i], b[i], c[i], d[i], geneIndex[i], this, pvalues[i]));
            }
            GetMinPValue();
        }


        public Group GroupA { get { return _groupA; } }

        public Group GroupB { get { return _groupB; } }

        public int GroupACount { get { return _groupA.Strains.Length; } }

        public int GroupBCount { get { return _groupB.Strains.Length; } }

        //counts for Group A vs Group B along all genes in the dataset

        public List<ContingencyTable> ContingencyTables { get { return _contingencyTables; } }

        public double MostExtremePValue { get { return _mostExtremePValue; } }

        public double MinPValue { get { return _minPValue; } }

        /// <summary>Threshold for determining what is positive or negative with the numerical data.</summary>
        public double Threshold { get { return _threshold; } }

        public double PValueThreshold { get; set; }

        public GroupCollection GroupCollection { get { return _groupCollection; } }

        /// <summary>Return the number of significant values for the comparison.</summary>
        public int CountSignificant
        {
            get
            {
                int count = 0;
                foreach (ContingencyTable ct in _contingencyTables)
                {
                    if (ct.GetFisher2TailPermutationTest() <= PValueThreshold)
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        public string GroupAName { get { return _groupA.Name; } }

        public string GroupBName { get { return _groupB.Name; } }

        private void GetMostExtremePValue()
        {
            int countGroupA = GroupACount;
            int countGroupB = GroupBCount;
            var ct = new ContingencyTable(countGroupA, 0, 0, countGroupB, -1, this);
            _mostExtremePValue = ct.GetHypergeometricProbability();
        }

        private void GetMinPValue()
        {
            _minPValue = _contingencyTables.Min(w => w.GetFisher2TailPermutationTest());
        }

        /// <summary>Create all of the GeneGroup members with the counts necessary for constructing contingency tables.</summary>
        /// <param name="data">Numerical data from numerical data file.</param>
        private void GetCounts()
        {
            int index = 0;
            //iterate through numeric data to extract only the information that is needed for each GeneGroup
            foreach (var dArr in _analysis.RawData)
            {
//reinitialize lists holding numeric data for group A and group B GeneGroup
                var grpA = new List<double>();
                var grpB = new List<double>();
                foreach (int i in _groupA.Strains)
                {
//add the numeric values corresponding to group A to grpA list of numeric values
                    grpA.Add(dArr[i]);
                }
                foreach (int i in _groupB.Strains)
                {
//add the numeric values corresponding to group B to grpB list of numeric values
                    grpB.Add(dArr[i]);
                }
                //create a new GeneGroup with the values that have been determined to be part of group A or B
                _geneGroups.Add(GetGeneGroup(index, grpA.ToArray(), grpB.ToArray()));
                _contingencyTables.Add(GetContingencyTable(_geneGroups[_geneGroups.Count - 1].A,
                                                           _geneGroups[_geneGroups.Count - 1].B,
                                                           _geneGroups[_geneGroups.Count - 1].C,
                                                           _geneGroups[_geneGroups.Count - 1].D,
                                                           _geneGroups.Count - 1, 
                                                           this));
                index++; //index to keep track of GeneGroup
            }
        }

        private void GetCounts(bool hasThresholds)
        {
            int index = 0;
            //iterate through numeric data to extract only the information that is needed for each GeneGroup
            for (int i = 0; i < _analysis.RawData.Count; i++)
            {
//reinitialize lists holding numeric data for group A and group B GeneGroup
                var grpA = new List<double>();
                var grpB = new List<double>();
                foreach (int j in _groupA.Strains)
                {
//add the numeric values corresponding to group A to grpA list of numeric values
                    grpA.Add(_analysis.RawData[i][j]);
                }
                foreach (int j in _groupB.Strains)
                {
//add the numeric values corresponding to group B to grpB list of numeric values
                    grpB.Add(_analysis.RawData[i][j]);
                }
                //create a new GeneGroup with the values that have been determined to be part of group A or B
                _geneGroups.Add(GetGeneGroup(index, _analysis.Thresholds[i], grpA.ToArray(), grpB.ToArray()));
                _contingencyTables.Add(GetContingencyTable(_geneGroups[_geneGroups.Count - 1].A,
                                                           _geneGroups[_geneGroups.Count - 1].B,
                                                           _geneGroups[_geneGroups.Count - 1].C,
                                                           _geneGroups[_geneGroups.Count - 1].D,
                                                           _geneGroups.Count - 1,
                                                           this));
                index++; //index to keep track of GeneGroup
            }
        }

        private GeneGroup GetGeneGroup(int index, IEnumerable<double> grpA, IEnumerable<double> grpB)
        {
            return new GeneGroup(index, _threshold, grpA, grpB);
        }

        private static GeneGroup GetGeneGroup(int index, double threshold, IEnumerable<double> grpA, IEnumerable<double> grpB)
        {
            return new GeneGroup(index, threshold, grpA, grpB);
        }

        private static ContingencyTable GetContingencyTable(int a, int b, int c, int d, int geneIndex, FETGroup fetGroup)
        {
            return new ContingencyTable(a, b, c, d, geneIndex, fetGroup);
        }

        private static ContingencyTable GetContingencyTable(int a, int b, int c, int d, int geneIndex, FETGroup fetGroup, double pValue)
        {
            return new ContingencyTable(a, b, c, d, geneIndex, fetGroup, pValue);
        }

        /// <summary>Calculate and store all Fisher statistics (2-tailed FET p-value, hypergeometric probability)</summary>
        public void CalcFisherValues()
        {
            var countdown = new CountdownEvent(1);
            foreach (ContingencyTable ct in _contingencyTables)
            {
                countdown.AddCount();
                ContingencyTable ct1 = ct;
                ThreadPool.QueueUserWorkItem(delegate
                                                 {
                                                     ct1.GetFisher2TailPermutationTest();
                                                     countdown.Signal();
                                                 });
            }

            countdown.Signal();
            countdown.Wait();

            GetMinPValue();
        }
    }
}