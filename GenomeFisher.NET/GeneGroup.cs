﻿using System.Collections.Generic;

namespace GenomeFisher.NET
{
    internal class GeneGroup
    {
        /// <summary>Gene index number.</summary>
        private readonly int _gene;

        /// <summary>Count for group A negative; B in a contingency table.</summary>
        private int _grpANegative;

        /// <summary>Count for group A positive; A in a contingency table.</summary>
        private int _grpAPositive;

        /// <summary>Count for group B negative; D in a contingency table.</summary>
        private int _grpBNegative;

        /// <summary>Count for group B positive; C in a contingency table.</summary>
        private int _grpBPositive;

        /// <summary>Construct a gene group from the values within group A and B.</summary>
        public GeneGroup(int index, double threshold, IEnumerable<double> grpA, IEnumerable<double> grpB)
        {
            _gene = index;
            CalculateCounts(threshold, grpA, grpB);
        }

        /// <summary>Return the gene index.</summary>
        public int GeneIndex { get { return _gene; } }

        /// <summary>Count for group A positive or A in a contingency table.</summary>
        public int A { get { return _grpAPositive; } }

        /// <summary>Count for group A negative or B in a contingency table.</summary>
        public int B { get { return _grpANegative; } }

        /// <summary>Count for group B positive or C in a contingency table.</summary>
        public int C { get { return _grpBPositive; } }

        /// <summary>Count for group B negative or D in a contingency table.</summary>
        public int D { get { return _grpBNegative; } }

        /// <summary>Get the counts associated with the numerical data for the gene for the strains that have been selected for each group.</summary>
        /// <param name="threshold">Threshold value for what is positive or negative for each group.</param>
        /// <param name="grpA">List of numerical values for group A.</param>
        /// <param name="grpB">List of numerical values for group B.</param>
        private void CalculateCounts(double threshold, IEnumerable<double> grpA, IEnumerable<double> grpB)
        {
            _grpANegative = 0;
            _grpAPositive = 0;
            _grpBNegative = 0;
            _grpBPositive = 0;

            foreach (double d in grpA)
            {
                if (d >= threshold)
                {
                    _grpAPositive++;
                }
                else
                {
                    _grpANegative++;
                }
            }
            foreach (double d in grpB)
            {
                if (d >= threshold)
                {
                    _grpBPositive++;
                }
                else
                {
                    _grpBNegative++;
                }
            }
        }
    }
}