﻿using System.Collections.Generic;

namespace GenomeFisher.NET
{
    public class Category
    {
        /// <summary>Counts of the amount of samples that correspond to that type in the sample information list.</summary>
        private readonly List<int> _counts;

        /// <summary>Name of the category, e.g. Location.</summary>
        private readonly string _name;

        /// <summary>List of strain groups for each type in the category.</summary>
        private readonly List<List<int>> _strains;

        /// <summary>Non-redundant list of sample information for the category.</summary>
        private readonly List<string> _types;

        /// <summary>Construct a new category with a list of sample information and a name for the category.</summary>
        /// <param name="name">Name of the category.</param>
        /// <param name="index">Index number of the category in the sample information list.</param>
        /// <param name="sampleInfo">All sample information line by line passed by reference from GenomeFisherAnalysis class.</param>
        public Category(string name, int index, ref List<string[]> sampleInfo)
        {
            _name = name;
            _types = new List<string>();
            _counts = new List<int>();
            _strains = new List<List<int>>();
            GetCountsAndSampleInfo(index, ref sampleInfo);
        }

        /// <summary>Name of the category, e.g. Location.</summary>
        public string Name { get { return _name; } }

        /// <summary>Non-redundant list of sample information for the category.</summary>
        public string[] Types { get { return _types.ToArray(); } }

        /// <summary>Counts of the amount of samples that correspond to that type in the sample information list.</summary>
        public int[] Counts { get { return _counts.ToArray(); } }

        /// <summary>Get the sample information list for the category and calculate the counts for the number of samples that belong to a certain group/type.</summary>
        /// <param name="index">Index of the category in the complete sample information list.</param>
        /// <param name="sampleInfo">Complete sample information list from GenomeFisherAnalysis.</param>
        private void GetCountsAndSampleInfo(int index, ref List<string[]> sampleInfo)
        {
            var tmp = new List<string>();
            //get the sample information list for the category
            for (int i = 0; i < sampleInfo.Count; i++)
            {
                tmp.Add(sampleInfo[i][index]);
            }
            //get the non-redundant list of sample information as well as the counts
            for (int i = 0; i < tmp.Count; i++)
            {
                string s = tmp[i];
                if (_types.Contains(s))
                {
                    int j = _types.IndexOf(s);
                    _counts[j]++;
                    //add strain index number to existing strain group
                    _strains[j].Add(i);
                }
                else if (!(_types.Contains(s)))
                {
                    _types.Add(s);
                    _counts.Add(1);
                    //create new strain group with current index value, i
                    var li = new List<int> {i};
                    _strains.Add(li);
                }
            }
        }

        public List<int> StrainGroup(int index)
        {
            return _strains[index];
        }
    }
}