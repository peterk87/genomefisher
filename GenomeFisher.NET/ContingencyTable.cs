﻿using System;
using System.Collections.Generic;

namespace GenomeFisher.NET
{
    /**
    * Implements a 2X2 contingency table data structure
    * Author - Guy Shani 2008
    * */
    public class ContingencyTable : IComparable
    {
        private const double EPSILON = double.Epsilon;

        /// <summary>Gene index. Gene string list stored in GenomeFisherAnalysis class.</summary>
        private readonly int _geneIndex;

        private readonly int _mIC;
        private readonly int _mID;
        private readonly int _mIa;
        private readonly int _mIb;
        private readonly int _mIn;
        private readonly string _mSAdditionalData;
        private double _mDFisher2TailValue;
        private readonly FETGroup _fetGroup;
        private double _mDHypergeometricValue;
        private double _mDMinimalPValue;
        private int _mIMaxPossibleA;
        private int _mIMinPossibleA;

        public ContingencyTable(int a, int b, int c, int d, int index, FETGroup fetGroup, double pValue = -1.0)
        {
            _geneIndex = index;
            _mIa = a;
            _mIb = b;
            _mIC = c;
            _mID = d;
            _mIn = a + b + c + d;
            _mDHypergeometricValue = -1.0;
            _mDFisher2TailValue = pValue;
            _fetGroup = fetGroup;
            _mIMinPossibleA = -1;
            _mIMaxPossibleA = -1;
            _mDMinimalPValue = 1.0;
            _mSAdditionalData = null;
        }

        /// <summary>Gene index. Gene string list stored in GenomeFisherAnalysis class.</summary>
        public int GeneIndex
        {
            get { return _geneIndex; }
        }

        #region IComparable Members

        /// <summary>We implement a comparable to allow the tables to be sorted by p-values.</summary>
        /// <param name="obj"></param>
        public int CompareTo(object obj)
        {
            if (obj is ContingencyTable)
            {
                var rOther = (ContingencyTable) obj;
                if (rOther.GetFisher2TailPermutationTest() < GetFisher2TailPermutationTest())
                    return 1;
                if (rOther.GetFisher2TailPermutationTest() > GetFisher2TailPermutationTest())
                    return -1;
            }
            else if (obj is double)
            {
                var d = (double) obj;
                if (d < GetFisher2TailPermutationTest())
                    return 1;
                if (d > GetFisher2TailPermutationTest())
                    return -1;
            }
            return 0;
        }

        #endregion

        public int GetA()
        {
            return _mIa;
        }

        public int GetB()
        {
            return _mIb;
        }

        public int GetC()
        {
            return _mIC;
        }

        public int GetD()
        {
            return _mID;
        }

        public int GetN()
        {
            return _mIn;
        }

        public override string ToString()
        {
            string sRest = string.Format("{0}\t{1}\t{2}\t{3}", GetA(), GetB(), GetC(), GetD());
            if (GetAdditionalData() != null)
                sRest += "\t" + GetAdditionalData();
            sRest += "\t" + FalseDiscoveryRate.Floor(GetFisher2TailPermutationTest());
            return sRest;
        }


        private string GetAdditionalData()
        {
            return _mSAdditionalData;
        }

        /// <summary>Validates that the marginals are all positive.</summary>
        private bool Validate()
        {
            return (_mIa + _mIb >= 0) && (_mIa + _mIC >= 0) && (_mIb + _mID >= 0) && (_mIC + _mID >= 0);
        }

        /// <summary>Returns 2 sided FET.  Uses "Fisher statistic" for determining whether counts are more extreme.
        /// See http://www.med.uio.no/imb/stat/two-by-two/methods.html.
        /// </summary>
        public double GetFisher2TailPermutationTest()
        {
            if (_mDFisher2TailValue >= 0.0)
                return _mDFisher2TailValue;
            if (!Validate())
                return 1;

            _mDFisher2TailValue = ComputeFisher2TailPermutationTest(GetHypergeometricProbability(), 1.0);
            if (_mDFisher2TailValue > 1.0)
                _mDFisher2TailValue = 1.0;

            return _mDFisher2TailValue;
        }

        /// <summary>Computes the Fisher 2 sided p-value. We iterate over all the premutations and sum the ones that have a lower probability (more extreme).
        /// We compute from scratch only a single hypergeometric probability - the probability of the "real" table.
        /// Then we iterate by incrementing the table and the probability (right side) and by decrementing the table and the probability (left side).
        /// The algorithm has the complexity of O(n), but usually runs much faster.
        /// Adding another possible optimization - when the p-value exceeds a cutoff - return 1. This is useful when we only need to know whether one value is larger than the other.
        /// When the values are too small to be represented by a double (less than 1-E302) the computation returns an upper bound on the real value.
        /// </summary>
        /// <param name="dObservedTableProbability"></param>
        /// <param name="dCutoff"></param>
        /// <returns></returns>
        private double ComputeFisher2TailPermutationTest(double dObservedTableProbability, double dCutoff)
        {
            double p0 = dObservedTableProbability;
            double p0Epsilon = p0*0.00001;
            double p = p0;
            int cRemiaingPermutations = GetMaxPossibleA() - GetMinPossibleA();

            _mDMinimalPValue = p0;

            if (Math.Abs(p0 - double.Epsilon) < EPSILON)
                return p0*cRemiaingPermutations; //an upper bound estimation


            //Iterate to the right side - increasing values of a
            ContingencyTable ctIterator = Next();
            double pt = IncrementalHypergeometricProbability(p0);
            while (ctIterator != null)
            {
                if (pt < _mDMinimalPValue)
                    _mDMinimalPValue = pt;
                if (p0 + p0Epsilon >= pt)
                {
                    p = p + pt;
                    if (p > dCutoff)
                        return 1.0;
                }

                pt = ctIterator.IncrementalHypergeometricProbability(pt);
                ctIterator = ctIterator.Next();

                if ((ctIterator == null) || (pt > p0Epsilon)) continue;
                pt *= (GetMaxPossibleA() - ctIterator.GetA() + 1);
                p += pt;
                ctIterator = null;
            }
            //Iterate to the left side - decreasing values of a

            ctIterator = Previous();
            pt = DecrementalHypergeometricProbability(p0);
            while (ctIterator != null)
            {
                if (pt < _mDMinimalPValue)
                    _mDMinimalPValue = pt;
                if (p0 + p0Epsilon >= pt)
                {
                    p = p + pt;
                    if (p > dCutoff)
                        return 1.0;
                }
                pt = ctIterator.DecrementalHypergeometricProbability(pt);
                ctIterator = ctIterator.Previous();

                if ((ctIterator != null) && (pt <= p0Epsilon))
                {
                    pt *= (ctIterator.GetA() - GetMinPossibleA());
                    p += pt;
                    ctIterator = null;
                }
            }

            return p;
        }

        /// <summary>Computes the Fisher scores for all the permutations in a single pass.
        /// The algorithm works by starting on one side (min a) and moving to the other side (max a).
        /// We compute all the probabilities that we encounter on the way incrementally.
        /// Then we sort the probabilities in increasing order and sum them up in that direction.
        /// The result is a mapping between the permutation probabilities and p-values.
        /// TODO - we may want to cache the resulting list in case of multiple calls
        /// </summary>
        public double[,] ComputeAllPermutationsScores()
        {
            int cPermutations = GetMaxPossibleA() - GetMinPossibleA() + 1;
            var alProbabilities = new List<double>();
            var adScores = new double[cPermutations,2];
            //We start from the table with the maximal value to avoid numeric computation problems
            ContingencyTable ctMaxValue = GetMaxValueTable();
            ContingencyTable ctIterator = ctMaxValue;
            double pStart = ctIterator.GetHypergeometricProbability();

            double pCurrent = pStart;
            int iCurrent;

            //iterate to the right side
            while (ctIterator != null)
            {
                //Add the probability of the current permutation to the list
                alProbabilities.Add(pCurrent);
                //Increment the probability
                pCurrent = ctIterator.IncrementalHypergeometricProbability(pCurrent);
                //Increment the table - will return null once a exceeds the max value
                ctIterator = ctIterator.Next();
            }

            //iterate to the left side
            ctIterator = ctMaxValue;
            pCurrent = ctIterator.DecrementalHypergeometricProbability(pStart);
            ctIterator = ctIterator.Previous();
            while (ctIterator != null)
            {
                //Add the probability of the current permutation to the list
                alProbabilities.Add(pCurrent);
                //Decrement the probability
                pCurrent = ctIterator.DecrementalHypergeometricProbability(pCurrent);
                //Decrement the table - will return null once a drops below the min value
                ctIterator = ctIterator.Previous();
            }

            //Sort the observed probabilities in increasing order
            alProbabilities.Sort();
            //BUGBUG - suspecting that we do not handle well identical entries. Not sure if this bug is occuring.
            double dSum = 0.0;
            //Sum the probabilities in increasing order, computing two sided p-values
            //BUGBUG - Not sure how to make this work for one sided tests.
            for (iCurrent = 0; iCurrent < alProbabilities.Count; iCurrent++)
            {
                pCurrent = alProbabilities[iCurrent];
                dSum += pCurrent;
                if (dSum > 1.0)
                    dSum = 1.0;
                adScores[iCurrent, 0] = pCurrent;
                adScores[iCurrent, 1] = dSum;
            }
            return adScores;
        }

        /// <summary>The actual computation is implemented in the HypergeometricProbability class</summary>
        /// <returns></returns>
        public double GetHypergeometricProbability()
        {
            if (_mDHypergeometricValue < 0.0)
                _mDHypergeometricValue = Misc.ComputeHypergeometricProbability(this);
            return _mDHypergeometricValue;
        }

        /// <summary>Computes hypergeometrical probability of the next contingency table:
        /// a b   =>   a+1 b-1
        /// c d   =>   c-1 d+1
        /// in an incremental manner using the current score
        /// </summary>
        /// <param name="dValue"></param>
        /// <returns></returns>
        private double IncrementalHypergeometricProbability(double dValue)
        {
            double a = _mIa, b = _mIb, c = _mIC, d = _mID; //must work with double to avoid overflow
            double dFactor = (b*c)/((a + 1.0)*(d + 1.0));
            double pt = dValue*dFactor;
            if (Math.Abs(pt - 0.0) < EPSILON)
                return double.Epsilon;
            return pt;
        }

        /// <summary>Computes hypergeometrical probability of the previous contingency table:
        /// a b   =>   a-1 b+1
        /// c d   =>   c+1 d-1
        /// in an incremental manner using the current score
        /// </summary>
        /// <param name="dValue"></param>
        /// <returns></returns>
        private double DecrementalHypergeometricProbability(double dValue)
        {
            double a = _mIa, b = _mIb, c = _mIC, d = _mID; //must work with double to avoid overflow
            double dFactor = (a*d)/((b + 1.0)*(c + 1.0));
            double pt = dValue*dFactor;
            if (Math.Abs(pt - 0.0) < EPSILON)
                return double.Epsilon;
            return pt;
        }

        //Returns the table that has the maximal probability and hence a p-value of 1
        //We do this by following the incremental process, checking to see when the coefficient of the increment factor drops below 1.
        private ContingencyTable GetMaxValueTable()
        {
            //can be done using binary search to speed things up
            double a = GetMinPossibleA();
            double b = _mIa + _mIb - a;
            double c = _mIa + _mIC - a;
            double d = _mIC + _mID - c;

            double dFactor = (b*c)/((a + 1.0)*(d + 1.0));

            while ((dFactor >= 1.0) && (a <= GetMaxPossibleA()))
            {
                a++;
                b = _mIa + _mIb - a;
                c = _mIa + _mIC - a;
                d = _mIC + _mID - c;
                dFactor = (b*c)/((a + 1.0)*(d + 1.0));
            }

            return new ContingencyTable((int) a, (int) b, (int) c, (int) d, _geneIndex, _fetGroup);
        }

        private int GetMaxPossibleA()
        {
            if (_mIMaxPossibleA <= 0)
                _mIMaxPossibleA = Math.Min(_mIa + _mIb, _mIa + _mIC);
            return _mIMaxPossibleA;
        }

        private int GetMinPossibleA()
        {
            if (_mIMinPossibleA <= 0)
                _mIMinPossibleA = Math.Max(_mIa - _mID, 0);
            return _mIMinPossibleA;
        }

        //Returns the next permutation with an incremented a
        private ContingencyTable Next()
        {
            if (_mIa == GetMaxPossibleA())
                return null;
            return new ContingencyTable(_mIa + 1, _mIb - 1, _mIC - 1, _mID + 1, _geneIndex, _fetGroup);
        }

        //Returns the previous permutation with a decremented a
        private ContingencyTable Previous()
        {
            if (_mIa == GetMinPossibleA())
                return null;
            return new ContingencyTable(_mIa - 1, _mIb + 1, _mIC + 1, _mID - 1, _geneIndex, _fetGroup);
        }

        public FETGroup FETGroup { get { return _fetGroup; } }
        //Can be returned only after calling computeFisher2TailPermutationTest
        public double GetMinimalAchievablePValue()
        {
            return _mDMinimalPValue;
        }
    }
}