﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace GenomeFisher.NET
{
    public static class Misc
    {
        #region Static Forms - listview form, inputbox

        /// <summary>Show an text input box.</summary>
        /// <param name="title">Title of the input box.</param>
        /// <param name="promptText">Prompt text.</param>
        /// <param name="value">Value within textbox.</param>
        /// <returns>Dialog result - OK or Cancel.</returns>
        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            var form = new Form();
            var label = new Label();
            var textBox = new TextBox();
            var buttonOk = new Button();
            var buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] {label, textBox, buttonOk, buttonCancel});
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        public static DialogResult ListviewSelect(string title, string prompt, string columnheader,
                                                  List<string> items, ref List<int> selected)
        {
            if (selected == null) throw new ArgumentNullException("selected");
            var form = new Form();
            var label = new Label();
            var lvw = new ListViewFF();
            var buttonOk = new Button();
            var buttonCancel = new Button();

            form.Text = title;
            label.Text = prompt;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(12, 9, 35, 13);
            lvw.SetBounds(12, 44, 260, 487);
            buttonOk.SetBounds(197, 547, 75, 23);
            buttonCancel.SetBounds(116, 547, 75, 23);

            lvw.Columns.Add(columnheader, 230);

            var lvList = new List<ListViewItem>();
            for (int i = 0; i < items.Count; i++)
            {
                var lvi = new ListViewItem(items[i]) {Selected = selected.Contains(i)};
                lvList.Add(lvi);
            }
            lvw.Items.AddRange(lvList.ToArray());

            label.AutoSize = true;
            //lvw.Anchor = lvw.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(300, 600);
            form.Controls.AddRange(new Control[] {label, lvw, buttonOk, buttonCancel});
            //form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();

            var tmp = new List<int>();
            foreach (int i in lvw.SelectedIndices)
            {
                tmp.Add(i);
            }

            selected = tmp;
            return dialogResult;
        }

        #endregion

        private const double EPSILON = double.Epsilon;

        public static string GetListViewData(ListViewFF lvw)
        {
            using (var sw = new StringWriter())
            {
                var headers = new List<string>();
                foreach (ColumnHeader col in lvw.Columns)
                {
                    headers.Add(col.Text);
                }
                sw.WriteLine(String.Join("\t", headers));
                foreach (ListViewItem lvi in lvw.Items)
                {
                    var line = new List<string>();
                    foreach (ListViewItem.ListViewSubItem subitem in lvi.SubItems)
                    {
                        line.Add(subitem.Text);
                    }
                    sw.WriteLine(String.Join("\t", line));
                }
                return sw.ToString();
            }
        }

        public static void OnListViewCopyToClipboard(KeyEventArgs args, ListViewFF lvw)
        {
            if (args.Control && args.KeyCode == Keys.C)
            {
                Clipboard.SetData(DataFormats.Text, GetListViewData(lvw));
            }
        }

        /// <summary>Open the context menu for a particular ListView if there is data in the ListView and that the GenomeFisherAnalysis object is not null.</summary>
        /// <param name="e">CancelEventArgs for ContextMenu</param>
        /// <param name="itemCount">Number of items in OLV.</param>
        /// <param name="data">GenomeFisherAnalysis object</param>
        public static void OpeningContextMenu(CancelEventArgs e, int itemCount, GenomeFisherAnalysis data)
        {
            if (data == null)
            {
                e.Cancel = true;
            }
            else if (itemCount == 0)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        ///Computes the hypergeometric probablity using the following factorization:
        /// (a+b)!(a+c)!(b+d)!(c+d)!     (a+b)!    (a+c)!    (c+d)!     (b+d)!
        /// ------------------------ =  ------- * ------- * -------- * --------
        ///       a!b!c!d!n!              a!b!       c!        d!         n!
        /// The assumption is that (a+b) is the smallest marginal. 
        /// A better implementation would check for the smallest marginal and factor according to it, but the current implementation seems fast enough.
        /// </summary>
        /// <param name="ct">ContigencyTable for which to calculate hypergeometric probability.</param>
        public static double ComputeHypergeometricProbability(ContingencyTable ct)
        {
            double pt = 1;
            double iFactorial;
            int a = ct.GetA(), b = ct.GetB(), c = ct.GetC(), d = ct.GetD();
            double iDenominator = a + b + c + d;
            double iMinDenominator = b + d;
            for (iFactorial = a + 1; iFactorial <= a + b; iFactorial++) // (a+b)!/a!b!
            {
                pt *= iFactorial/(iFactorial - a);
                while ((pt > 1) && (iDenominator > iMinDenominator))
                {
                    pt /= iDenominator;
                    iDenominator--;
                }
            }
            for (iFactorial = c + 1; iFactorial <= a + c; iFactorial++) // (a+c)!/c! 
            {
                pt *= iFactorial;
                while ((pt > 1) && (iDenominator > iMinDenominator))
                {
                    pt /= iDenominator;
                    iDenominator--;
                }
            }
            for (iFactorial = d + 1; iFactorial <= c + d; iFactorial++) // (c+d)!/d! 
            {
                pt *= iFactorial;
                while ((pt > 1) && (iDenominator > iMinDenominator))
                {
                    pt /= iDenominator;
                    iDenominator--;
                }
            }

            if (Math.Abs(pt - 0.0) < EPSILON) //underflow
                return Double.Epsilon;

            while ((iDenominator > iMinDenominator) && (pt > 0.0))
            {
                pt /= iDenominator;
                if (Math.Abs(pt - 0.0) < EPSILON) //underflow
                    return Double.Epsilon;
                iDenominator--;
            }
            if (pt > 1.0) //numerical error
                pt = 1.0;

            return pt;
        }
    }
}