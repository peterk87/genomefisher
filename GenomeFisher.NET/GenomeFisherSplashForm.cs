﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GenomeFisher.NET
{
    public partial class GenomeFisherSplashForm : Form
    {
        public GenomeFisherSplashForm()
        {
            InitializeComponent();
            label1.Text = "Genome Fisher .NET is a statistical analysis tool using the Fisher Exact Test.\n" +
                          "Programming:  Peter Kruczkiewicz\n" +
                          "Design & Conception:  Dr Eduardo Taboada\n" +
                          "Public Health Agency of Canada (2011)";
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmGenomeFisherSplash_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }
    }
}
